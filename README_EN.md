

<!-- TOC -->

- [Easy Snippets](#easy-snippets)
    - [Index Page](#index-page)
    - [Snippets Editor](#snippets-editor)
- [Detail](#detail)
    - [Storage](#storage)
    - [Build](#build)
        - [Mac OS](#mac-os)
        - [Build for Win](#build-for-win)
        - [About Icon](#about-icon)
    - [Development](#development)
- [Dependency](#dependency)
- [Support](#support)
- [Version](#version)
- [Feature && Issue](#feature--issue)

<!-- /TOC -->

![nodejs](https://img.shields.io/badge/Nodejs-v12.16.3%5E-green)&nbsp;&nbsp;![VUE](https://img.shields.io/badge/VUE-2.0-green)&nbsp;&nbsp;![CodeMirror](https://img.shields.io/badge/CodeMirror-4.0.6-green)

> Easy code snippets for software enginer!
> Simple！ Easy to use!
# Easy Snippets
实际的开发工作中,往往会有许许多多的零散**代码块**。例如某个经典的`utils` 、某个特定环境的`一段配置` 、一个重要的`脚本实现`，又或者是一些`优雅的算法实现`。没有一个方便的位置管理零散的知识点,大多数的情况下，我们会选择存储在本地的`md`  or 其他可以存储的文件中。Easy Snippets，方便大家记录&使用这些“伟大的知识碎片”。

## Index Page
- 首页展示当前存储的所有snippets，上方可使用关键词搜索及snippets的语言类型过滤。点击单个snippets后方的<kbd>Clipboard</kbd>按键，该snippets中的代码内容将被复制到剪贴板。
![snippets](./doc/main_page.png)

## Snippets Editor
- 点击首页的<kbd>Create</kbd>或列表项后的<kbd>Edit</kbd>进入snippet编辑页。内部嵌入一个codemirror editor，可能还有没有加上去的高亮语言类型，请告诉我！
![editor](./doc/editor_page.png)

- 支持以下语言/框架的高亮展示。

|Language|Options|
|---|---|
|JavaScript|--|
|TypeScript|--|
|SQL|--|
|Java|--|
|Python|--|
|css/sass/less|html样式文件|
|html|--|
|xml|--|
|yaml|常见于各类配置文件，例如springboot的各类yaml配置文件|
|Properties|常见为各类配置文件|
|shell|脚本。例如程序启动脚本|
|Dart|fultter程序开发使用语言|
|Vue|前端框架|
|Go|--|
|Groovy|脚本语言。|


# Detail
- 纯local实现。无需联网。

## Storage
- 目前存储在内部的indexDB中。借助`Dexie.js`操作IndexDB.不考虑接入sqlite3 or其他外部数据库，beacause....觉得太麻烦了，不够轻量。。当前目前的搜索部分实现的有点儿 cuo。感兴趣的同学可以来优化下，hhh.

## Build
-  为Electron设置cnpm mirror.
```bash
    npm config set ELECTRON_MIRROR https://npm.taobao.org/mirrors/electron/
```
- 更新依赖
    ```bash
        git clone https://gitee.com/black-flash-oreo/easy-snippets.git
        cd easy-snippets
        yarn
    ```
### Mac OS
- for mac OS `.dmg`.
    ```bash
    # do packaging 
    npm run package

    # build
    npm run build
    ```

### Build for Win
- `npm run buildWin`

### About Icon
```bash
# Using electron-icon-builder
cnpm install -g electron-icon-builder
# generate icon for all size
electron-icon-builder --input=/absolute/path/file.png --output=./relative/path/to/folder
```

## Development

```bash
# clone to local dir.
git clone xxx
cd easy-snippets
# Using yarn get all dependencies.
yarn
npm run dev
```

# Dependency
<sup>Order in alphabetical order 🐶</sup>
- CodeMirror(一款伟大的H5代码高亮编辑器！😄)
- Dexie.js（操作IndexDB的神器！😏）
- Electron（H5开发桌面端的神器！😁）
- Vue（不用说了吧！😁）
- vue-codemirror(神器！😁)
- vuex-electron(神器！😁)
- ...<sup>others nested dependencies!!🧎‍♀️🧎‍♀️🧎‍♀️</sup>

> THANKS!!!!!

# Support 
- 如果觉得有用，请我喝杯咖啡吧，哈哈哈哈

    <img src="./doc/ali-pay.png" width = "120px" height = "auto" alt="alipay" align=center />

# Version
> 可直接在RELEASE页面下载安装包，也可以在百度网盘下载（如果你有V3会员的话，😂😂😂😂）

> 目前更新的话！！！还是只能下载全量安装包安装！！！经费有限！！！等有钱了搞增量更新版本服务器！！！

- V2.0.0 <sup style='color: #315efb'>&nbsp;In progress ！！！！</sup>

    - 支持单个snippets的导入导出&nbsp;<sup>直接生成相应后缀的文件</sup>
    - 支持全库的export/import&nbsp;<sup>方便多环境情况下的**数据迁移**</sup>
    - 支持snippets备份&nbsp;<sup>支持时间纬度的定时snippets**滚动备份**</sup>
    - 支持snippets多级分组&nbsp;

- V1.0.1<sup style='color: green'>Available!</sup>
    1. 修复：editor页主题切换时，编辑器主题样式不变问题
    2. 修复：ALL过滤条件无效问题
    3. 优化：返回列表页会保留所有过滤条件
    4. 优化： 增加新版本提醒通知的点击跳转RELEASE页面功能

- V1.0.0<sup style='color: green'>Available!</sup>
    - [RELEASE](https://gitee.com/black-flash-oreo/easy-snippets/releases/v1.0.0)下载
    - 支持MAC/Window64
    - 支持多种语言的snippets创建
    - 支持snippets内容检索
    - 展示效果支持主题切换

- [BaiDuNetDisk](https://pan.baidu.com/s/1JwaIR-WO975Ir7DIoZ0zrw) ,PWD: `3g3s`

# Feature && Issue
- 有新的需求 & BUG 请直接 `new issue`