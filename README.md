

<!-- TOC -->

- [Easy Snippets](#easy-snippets)
    - [Index Page](#index-page)
    - [Snippets Editor](#snippets-editor)
    - [Theme](#theme)
        - [Black](#black)
        - [Light](#light)
    - [Storage](#storage)
    - [Dev](#dev)
    - [Build](#build)
        - [Mac OS](#mac-os)
        - [Win](#win)
    - [Linux](#linux)
        - [Icon Builder](#icon-builder)
- [Dependency](#dependency)
- [Support](#support)
- [Version](#version)
- [Download artifacts](#download-artifacts)
- [Feature && Issue](#feature--issue)

<!-- /TOC -->

![nodejs](https://img.shields.io/badge/Nodejs-v12.16.3%5E-green)&nbsp;&nbsp;![VUE](https://img.shields.io/badge/VUE-2.0-green)&nbsp;&nbsp;![CodeMirror](https://img.shields.io/badge/CodeMirror-4.0.6-green)

> Easy code snippets for software enginer!

> 为了使用方便。一直会秉持 **“简洁”**、**“实用”**的 更新和设计原则！因为Coder们的💻已经跑了很多程序了，过度的渲染毫无意义，只保留最实用的功能！！！！整个APP基本无图标，运行期间也不会联网发什么数据（只会在启动时借助gitee的rest API，获取下有无行新版本）。

> **<div  style='color:green'>使用请直接去[release下载，或者本文档末尾有baidu网盘分享](#download-artifacts)链接，无需编译！可能还有未覆盖的平台，请直接new issue告诉我。</div >**
# Easy Snippets
实际的开发工作中,往往会有许许多多的零散**代码块**。例如
    - 某个经典的`utils`；
    - 某个特定环境的`一段配置`；
    - 一个重要的`脚本实现`；
    - 一些`优雅的算法实现`
没有一个方便的位置管理超级零散的知识点。很久的一段时间，笔者都是在公司内部仓库创建一个`markdown`项目，来维护这些零散的知识点，带来的问题诸多，结构混乱，查看麻烦，无分类。故**Easy Snippets**应运而生，解决问题，提高效率！

## Index Page
- 首页展示当前存储的所有snippets，上方可使用关键词搜索及snippets的语言类型过滤。点击单个snippets后方的<kbd>Clipboard</kbd>按键，该snippets中的代码内容将被复制到剪贴板。这里只支持通过语言类型&关键字查询，因为snippet实在无逻辑关系，平时的开发工作中也不大有空去填写那么多的对话框。
![snippets](./doc/main_page.png)

## Snippets Editor
- 点击首页的<kbd>Create</kbd>或列表项后的<kbd>Edit</kbd>进入snippet编辑&&新建页。内部嵌入一个codemirror editor，可能还有没有加上去的高亮语言类型，请告诉我！表单为三个字段➕代码块，你可以选择是否填写描述信息。就希望能够快速记录snippets，不需要过多的无关设置，简洁！高效！
![editor](./doc/editor_page.png)

- 支持以下语言/框架的高亮展示。

|Language|Options|
|---|---|
|JavaScript|--|
|TypeScript|--|
|SQL|--|
|Java|--|
|Python|--|
|css/sass/less|样式|
|html|前端|
|xml|不用说了，常见于各种配置信息。|
|yaml|常见于各类配置文件，例如springboot的各类yaml配置文件|
|Properties|常见为各类配置文件|
|shell|脚本。例如程序启动脚本|
|Dart|fultter程序开发使用语言|
|Vue|前端框架|
|Go|--|
|Groovy|脚本语言。|

##  Theme
- 支持 “暗黑金”/“春节蓝”两种主题,可点击页面上的<kbd>Light|Black</kbd> 切换。主题效果如下，

### Black
![snippets](./doc/main_page.png)
![editor](./doc/editor_page.png)

### Light
![snippets](./doc/main_page_light.png)
![editor](./doc/editor_page_light.png)


## Storage
- 目前存储在内部的indexDB中。借助`Dexie.js`操作IndexDB.不考虑接入sqlite3 or其他外部数据库，因为够用也不需要暴露太多东西处理。

## Dev

```bash
# clone to local dir.
git clone xxx
cd easy-snippets
# Using yarn install all dependencies.
yarn
npm run dev
```

## Build
-  为Electron设置国内源
    ```bash
        npm config set ELECTRON_MIRROR https://npm.taobao.org/mirrors/electron/
    ```
- 更新依赖
    ```bash
        git clone https://gitee.com/black-flash-oreo/easy-snippets.git
        cd easy-snippets
        yarn
    ```
### Mac OS
- for mac OS `.dmg`.
    ```bash
    # do packaging 
    npm run package

    # build
    npm run build
    ```

### Win
- `npm run buildWin`

## Linux
- `npm run buildLinux`

### Icon Builder
```bash
# Using electron-icon-builder
cnpm install -g electron-icon-builder
# generate icon for all size
electron-icon-builder --input=/absolute/path/file.png --output=./relative/path/to/folder
```

# Dependency
<sup>Order in alphabetical order 🐶</sup>
- CodeMirror(一款伟大的H5代码高亮编辑器！😄)
- Dexie.js（操作IndexDB的神器！😏）
- Electron（H5开发桌面端的神器！😁）
- Vue（不用说了吧！😁）
- vue-codemirror(神器！😁)
- vuex-electron(神器！😁)
- ...<sup>others nested dependencies!!🧎‍♀️🧎‍♀️🧎‍♀️</sup>

> THANKS!!!!!

# Support 
- 如果觉得有用，请我喝杯咖啡吧，哈哈哈哈

    <img src="./doc/ali-pay.png" width = "120px" height = "auto" alt="alipay" align=center />

# Version

- V2.0.0 <sup style='color: #315efb'>&nbsp;In progress ！！！！</sup>
    - 支持单个snippets的导入导出&nbsp;<sup>直接生成相应后缀的文件</sup>
    - 支持全库的export/import&nbsp;<sup>方便多环境情况下的**数据迁移**</sup>
    - 支持snippets备份&nbsp;<sup>支持时间纬度的定时snippets**滚动备份**</sup>
    - 支持snippets多级分组&nbsp;

- V1.0.1<sup style='color: green'>Available!</sup>
    1. Fixed：editor页主题切换时，编辑器主题样式不变问题
    2. Fixed：ALL过滤条件无效问题
    3. Feature：返回列表页会保留所有过滤条件
    4. Feature： 增加新版本提醒通知的点击跳转RELEASE页面功能
    5. Feature: 支持snippets排序字段配置

- V1.0.0<sup style='color: green'>Available!</sup>
    - [RELEASE](https://gitee.com/black-flash-oreo/easy-snippets/releases/v1.0.0)下载
    - 支持MAC/Window64/linux
    - 支持多种语言的snippets创建
    - 支持snippets内容检索
    - 展示效果支持主题切换

# Download artifacts
- [Gitee Release Page](https://gitee.com/black-flash-oreo/easy-snippets/releases)
- [BaiDuNetDisk](https://pan.baidu.com/s/1JwaIR-WO975Ir7DIoZ0zrw) ,PWD: `3g3s`

# Feature && Issue
- 有新的需求 & BUG 请直接 `new issue`。谢谢🙏